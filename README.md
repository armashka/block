# Test case

CLI to get biggest difference in balance.

## Installation and Run
Your Api Token [reqired]
```
go run main.go --token "YOUR_TOKEN" 
```
Also you can pass block difference [not required] (default 100)
```
go run main.go --token "YOUR_TOKEN" --block 200
```

## Tech

- [zap] - Blazing fast, structured, leveled logging in Go.
- [fasthttp] - Package fasthttp provides fast HTTP server and client API.
- [easyjson] - Package easyjson provides a fast and easy way to marshal/unmarshal Go structs to/from JSON without the use of reflection. In performance tests, easyjson outperforms the standard encoding/json package by a factor of 4-5x, and other JSON encoding packages by a factor of 2-3x.
- [cobra] - obra is a library for creating powerful modern CLI applications.


