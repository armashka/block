package internal

import (
	"fmt"
	"go.uber.org/zap"
	"test/internal/config"
	"test/internal/service/block"
	"test/internal/service/business"
	"test/internal/service/general"
	"time"
)

func Start() {

	if config.GlobalConfig.Block.Token == "" {
		fmt.Println("Please provide your API token via flags (--token).")
		return
	}
	logger, _ := zap.NewProduction()
	defer logger.Sync() // flushes buffer, if any

	// init services
	genSrv := general.NewServiceGeneral(logger)
	blockSrv := block.NewServiceBlock(genSrv, config.GlobalConfig.Block, logger)
	mainSrv := business.NewServiceBusiness(blockSrv, config.GlobalConfig.BlockCount, logger)

	start := time.Now()
	diff, max, err := mainSrv.GetHighestDiff()
	if err != nil {
		fmt.Println("error", err.Error())
		return
	}

	fmt.Println(fmt.Sprintf("\n\nThe biggest an account balance difference happened at %s to value %v (time taken -> %s)", diff, max.Uint64(), time.Since(start)))
}
