package config

var (
	GlobalConfig Config
)

type Config struct {
	Block      Block  `json:"block"`
	BlockCount uint64 `json:"block_count"`
}

type Block struct {
	MainURL     string `json:"main_url"`
	Token       string `json:"-"`
	Balance     string `json:"balance_url"`
	BlockNumber string `json:"block_number"`
	BlockInfo   string `json:"block_info"`
}
