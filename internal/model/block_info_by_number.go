package model

// BlockInfoByNumber implements Marshaler and Unmarshaler (used easyjson package to generate code since performs faster)
type BlockInfoByNumber struct {
	Result struct {
		Transactions []struct {
			From string `json:"from"`
			To   string `json:"to"`
		} `json:"transactions"`
	} `json:"result"`
}

func (v BlockInfoByNumber) GetDistinctAddresses() []string {
	excludeCopies := make(map[string]struct{})
	addresses := make([]string, 0)

	for _, transaction := range v.Result.Transactions {
		// addresses may be repeated
		if _, ok := excludeCopies[transaction.To]; !ok {
			addresses = append(addresses, transaction.To)
			excludeCopies[transaction.To] = struct{}{}
		}

		if _, ok := excludeCopies[transaction.From]; !ok {
			addresses = append(addresses, transaction.From)
			excludeCopies[transaction.From] = struct{}{}
		}
	}

	return addresses
}
