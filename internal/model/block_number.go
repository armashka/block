package model

// BlockNumberAndAddressBalanceResponse implements Marshaler and Unmarshaler (used easyjson package to generate code since performs faster)
type BlockNumberAndAddressBalanceResponse struct {
	Result string `json:"result"`
}
