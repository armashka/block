package model

// GeneralRequest implements Marshaler and Unmarshaler (used easyjson package to generate code since performs faster)
type GeneralRequest struct {
	JsonRPC string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
	ID      string        `json:"id"`
}
