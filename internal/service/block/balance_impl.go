package block

import (
	"github.com/valyala/fasthttp"
	"go.uber.org/zap"
	"math/big"
	"test/internal/model"
	"test/pkg/converters"
)

func (s service) GetBalance(address string, number string) (*big.Int, error) {
	params := []interface{}{address, number}

	req := model.GeneralRequest{
		JsonRPC: "2.0",
		Method:  s.conf.Balance,
		Params:  params,
		ID:      "getblock.io",
	}

	json, err := req.MarshalJSON()
	if err != nil {
		s.logg.Error("could not marshal req body", zap.Error(err))
		return &big.Int{}, err
	}

	headers := make(map[string]string)
	headers[header] = s.conf.Token
	headers[fasthttp.HeaderContentType] = "application/json"

	resp := model.BlockNumberAndAddressBalanceResponse{}

	err = s.generalSrv.Requester(s.conf.MainURL, headers, json, &resp)
	if err != nil {
		s.logg.Error("got requester err", zap.Error(err))
		return &big.Int{}, err
	}

	distance := new(big.Int)
	distance.SetString(converters.HexaNumberToInteger(resp.Result), 16)

	return distance, nil
}
