package block

import (
	"go.uber.org/zap"
	"math/big"
	"test/internal/config"
	"test/internal/model"
	"test/internal/service/general"
)

const header = "x-api-key"

type ServiceBlock interface {
	GetBlockNumber() (string, error)
	GetBlockInfo(number string) (model.BlockInfoByNumber, error)
	GetBalance(address string, number string) (*big.Int, error)
}

type service struct {
	generalSrv general.ServiceGeneral
	conf       config.Block
	logg       *zap.Logger
}

func NewServiceBlock(generalSrv general.ServiceGeneral, conf config.Block, logg *zap.Logger) ServiceBlock {
	return service{
		generalSrv: generalSrv,
		conf:       conf,
		logg:       logg,
	}
}
