package block

import (
	"github.com/valyala/fasthttp"
	"go.uber.org/zap"
	"test/internal/model"
)

func (s service) GetBlockInfo(number string) (model.BlockInfoByNumber, error) {

	params := []interface{}{number, true}

	req := model.GeneralRequest{
		JsonRPC: "2.0",
		Method:  s.conf.BlockInfo,
		Params:  params,
		ID:      "getblock.io",
	}

	json, err := req.MarshalJSON()
	if err != nil {
		s.logg.Error("could not marshal req body", zap.Error(err))
		return model.BlockInfoByNumber{}, err
	}

	resp := model.BlockInfoByNumber{}

	headers := make(map[string]string)
	headers[header] = s.conf.Token
	headers[fasthttp.HeaderContentType] = "application/json"

	err = s.generalSrv.Requester(s.conf.MainURL, headers, json, &resp)
	if err != nil {
		s.logg.Error("got requester err", zap.Error(err))
		return model.BlockInfoByNumber{}, err
	}

	return resp, nil
}
