package general

import (
	"encoding/json"
	"go.uber.org/zap"
)

// ServiceGeneral service to make request from single point
type ServiceGeneral interface {
	Requester(url string, headers map[string]string, body []byte, unmarshaler json.Unmarshaler) error
}

type generalService struct {
	logg *zap.Logger
}

func NewServiceGeneral(logg *zap.Logger) ServiceGeneral {
	return generalService{
		logg: logg,
	}
}
