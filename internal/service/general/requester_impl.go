package general

import (
	"encoding/json"
	"errors"
	"github.com/valyala/fasthttp"
	"go.uber.org/zap"
)

func (s generalService) Requester(url string, headers map[string]string, body []byte, unmarshaler json.Unmarshaler) error {

	// used fasthttp with fewer allocations (pool used)
	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseRequest(req)   // <- do not forget to release
	defer fasthttp.ReleaseResponse(resp) // <- do not forget to release

	req.SetRequestURI(url)
	req.SetBody(body)

	// passing token
	for keey, val := range headers {
		req.Header.Set(keey, val)
	}

	err := fasthttp.Do(req, resp)
	if err != nil {
		s.logg.Error("could not make request", zap.Error(err), zap.String("url", url))
		return err
	}

	if resp.StatusCode() != 200 {
		s.logg.Error("status code not ok", zap.Int("got status", resp.StatusCode()), zap.String("url", url))
		return errors.New("status not ok error")
	}

	err = unmarshaler.UnmarshalJSON(resp.Body())
	if err != nil {
		return err
	}

	return nil
}
