package business

import (
	"fmt"
	"go.uber.org/zap"
	"math/big"
	"strconv"
	"sync"
	"test/pkg/converters"
)

func (s serviceBusiness) GetHighestDiff() (address string, max *big.Int, err error) {

	// getting last block number
	latestBlockNum, err := s.block.GetBlockNumber()
	if err != nil {
		return "", &big.Int{}, err
	}

	s.logg.Info("latest block", zap.String("hex", latestBlockNum))

	// using simple ParseUint since not overflows
	latestBlockInt, err := strconv.ParseUint(converters.HexaNumberToInteger(latestBlockNum), 16, 64)
	if err != nil {
		s.logg.Error("could not parse block number", zap.Error(err))
		return "", &big.Int{}, err
	}
	s.logg.Info("latest block", zap.Uint64("int", latestBlockInt))

	oldInt := latestBlockInt - s.blockCount
	// added prefix "0x", since get block error
	oldBlockNum := fmt.Sprintf("0x%x", oldInt)

	s.logg.Info("old block", zap.String("hex", oldBlockNum))
	s.logg.Info("old block", zap.Uint64("int", oldInt))

	// get all block transactions and addresses (from and to)
	info, err := s.block.GetBlockInfo(latestBlockNum)
	if err != nil {
		s.logg.Error("could not get block info", zap.Error(err))
		return "", &big.Int{}, err
	}

	// getting distinct addresses without copies
	addresses := info.GetDistinctAddresses()
	s.logg.Info("all distinct", zap.Strings("addresses", addresses))

	// getting balances for addresses from diff blocks
	latest, old := s.getAllBalance(addresses, latestBlockNum, oldBlockNum)

	// comparing results
	add, max := s.compareAndFindMaxDiff(latest, old)
	return addresses[add], max, nil
}

func (s serviceBusiness) getAllBalance(addresses []string, blockNumberLatest, blockNumberOld string) (latest, old []*big.Int) {

	// making balance arrays for both blocks
	latest = make([]*big.Int, len(addresses))
	old = make([]*big.Int, len(addresses))

	wg := &sync.WaitGroup{}
	// there will no concurrent writes since we are passing index
	// each goroutine will handle given address and write result to given index
	for i, address := range addresses {
		wg.Add(2)
		go s.getAllBalanceInternal(wg, address, blockNumberLatest, latest, i)
		go s.getAllBalanceInternal(wg, address, blockNumberOld, old, i)
	}
	wg.Wait()

	return
}

func (s serviceBusiness) getAllBalanceInternal(wg *sync.WaitGroup, address string, blockNumber string, balances []*big.Int, index int) {
	defer wg.Done()

	// just getting balance
	balance, err := s.block.GetBalance(address, blockNumber)
	if err != nil {
		s.logg.Error("could not get balance", zap.Error(err))
		return
	}

	// concurrent safe write
	balances[index] = balance
}

func (s serviceBusiness) compareAndFindMaxDiff(new []*big.Int, old []*big.Int) (index int, max *big.Int) {
	// just comparing and getting the biggest difference
	max = big.NewInt(0)
	for i, value := range new {
		temp := big.NewInt(0).Sub(value, old[i])
		temp = big.NewInt(0).Abs(temp)

		if temp.Cmp(max) == 1 {
			max.SetBytes(temp.Bytes())
		}
	}

	return
}
