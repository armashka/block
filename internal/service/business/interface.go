package business

import (
	"go.uber.org/zap"
	"math/big"
	"test/internal/service/block"
)

type ServiceBusiness interface {
	GetHighestDiff() (address string, max *big.Int, err error)
}

type serviceBusiness struct {
	block      block.ServiceBlock
	blockCount uint64
	logg       *zap.Logger
}

func NewServiceBusiness(block block.ServiceBlock, blockCount uint64, logg *zap.Logger) ServiceBusiness {
	return serviceBusiness{
		block:      block,
		blockCount: blockCount,
		logg:       logg,
	}
}
