/*
Copyright © 2022 Arman Bimak armanbimak27@gmail.com
*/
package cmd

import (
	"os"
	"test/internal"
	"test/internal/config"

	"github.com/spf13/cobra"
)

// using cobra since later we can easily extend our program with new command
// not only getting diff problem
var rootCmd = &cobra.Command{
	Use:    "CLI",
	PreRun: loadConfig,
	Run: func(cmd *cobra.Command, args []string) {
		internal.Start()
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().StringVar(&config.GlobalConfig.Block.Token, "token", "ss", "Api token for auth requests")
	rootCmd.Flags().Uint64Var(&config.GlobalConfig.BlockCount, "block", 100, "Count")
	err := rootCmd.MarkFlagRequired("token")
	if err != nil {
		return
	}
}
