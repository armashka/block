package cmd

import (
	"encoding/json"
	"github.com/spf13/cobra"
	"io"
	"log"
	"os"
	"test/internal/config"
)

func loadConfig(cmd *cobra.Command, args []string) {
	cfgFile, fileErr := os.Open("config.json")

	if fileErr != nil {
		log.Fatalln("could not find file")
	}
	raw, err := io.ReadAll(cfgFile)
	if err != nil {
		log.Fatalln("could not read config file")
	}

	err = json.Unmarshal(raw, &config.GlobalConfig)
	if err != nil {
		log.Fatalln("could not unmarshal config file")
	}
}
