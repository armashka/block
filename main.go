/*
Copyright © 2022 Arman Bimak armanbimak27@gmail.com

*/
package main

import "test/cmd"

func main() {
	cmd.Execute()
}
