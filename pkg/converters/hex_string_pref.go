package converters

import "strings"

func HexaNumberToInteger(hexaString string) string {
	// using this func since ParseInt returns "syntax error" when hex num starts with "0x"
	numberStr := strings.Replace(hexaString, "0x", "", -1)
	numberStr = strings.Replace(numberStr, "0X", "", -1)
	return numberStr
}
